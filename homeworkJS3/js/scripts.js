'use strict'


let numFirst;
let numSecond;
let operation;

do {
    numFirst = prompt("Enter first number", numFirst);
} while (isNaN(+numFirst) || numFirst === null || numFirst.trim() === "");

do {
    numSecond = prompt("Enter second number", numSecond);
} while (isNaN(+numSecond) ||numSecond === null ||numSecond.trim() === "");

do {
    operation = prompt("Enter operation(+, -, * or /)", operation);
} while (!operation.match(/^(\+|\-|\*|\/)$/) || operation === null);

function mathFunc(numFirst, numSecond, operation) {
    switch (operation) {
        case "+":
          return +numFirst + +numSecond;
          break;
        case "-":
          return +numFirst - +numSecond;
          break;
        case "*":
          return +numFirst * +numSecond;
          break;
        case "/":
          return +numFirst / +numSecond;
          break;
    }
}
console.log(mathFunc(numFirst, numSecond, operation));