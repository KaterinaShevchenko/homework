'use strict'

function filterBy(array = [], type) {
   return array.filter((newArray) =>
   typeof newArray !== type);
};
console.log(filterBy(["hello", "world", 23, "23", null], "number"));